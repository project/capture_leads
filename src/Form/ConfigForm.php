<?php

namespace Drupal\capture_leads\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm provides config settings for capture_leads.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configManager = $container->get('config.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'capture_leads.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('capture_leads.config');

    $form['redirect_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect Domain'),
      '#description' => $this->t(
        'The domain only for the redirect process. The path will be supplied to the endpoint.'
      ),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('redirect_domain'),
    ];

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug Mode'),
      '#description' => $this->t(
        'Checking this box returns more verbose error messages for debugging. This is NOT for use in production environment.'
      ),
      '#default_value' => $config->get('debug_mode'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('capture_leads.config')
      ->set('redirect_domain', $form_state->getValue('redirect_domain'))
      ->set('debug_mode', $form_state->getValue('debug_mode'))
      ->save();
  }

}
