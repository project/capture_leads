<?php

namespace Drupal\capture_leads\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\capture_leads\Response\ErrorResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for test_api routes.
 */
class APIController extends ControllerBase {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * DefaultController constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('capture_leads'),
    );
  }

  /**
   * Callback for `capture_leads/api/post.json` API method.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Incoming API POST request.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse|ErrorResponse
   *   Returns a Redirect or Error response as appropriate
   */
  public function postLead(Request $request) {
    $config = $this->config('capture_leads.config');
    $redirectDomain = $config->get('redirect_domain');

    // Check if the redirectUrl key exists and handle it gracefully regardless.
    try {
      if ($redirectPath = $request->get('redirectUrl')) {
        /* TODO: Build out the data collection and storage calls */
        // $memberId = $request->get('memberId');
        // $cellCode = $request->get('cellCode');
        // $campaignCode = $request->get('campaignCode');
        return new TrustedRedirectResponse($redirectDomain . $redirectPath);
      }
      else {
        // No redirectUrl?  Drop them on the homepage instead for good UX.
        return new TrustedRedirectResponse($redirectDomain);
      }
    }
    catch (ClientException $exception) {
      return $this->prepareErrorResponse($exception);
    }

  }

  /**
   * Process and prepare error object.
   *
   * @param object $exception
   *   Exception object.
   *
   * @return \Drupal\capture_leads\Response\ErrorResponse
   *   An error object.
   */
  private function prepareErrorResponse($exception) {
    $config = $this->config('capture_leads.config');
    $debugMode = $config->get('debug_mode');

    $message = $exception->getMessage();

    $response = json_decode($exception->getResponse()->getBody());

    // Check if response has any other messages.
    if (!empty($response->errorCauses) && !$debugMode) {
      $message .= "We've experienced a problem! Possible causes: " . print_r($response->errorCauses, TRUE);
    }
    else {
      $message .= "We've experienced a problem! Please contact a system admin for help.";
    }
    $this->logger->error($message);

    return new ErrorResponse($response);
  }

}
