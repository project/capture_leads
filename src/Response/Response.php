<?php

namespace Drupal\capture_leads\Response;

/**
 * Capture Leads response.
 *
 * @package Drupal\capture_leads\Response
 */
abstract class Response {

  /**
   * Error summary.
   *
   * @var string
   */
  protected $errorSummary;

  /**
   * Error code.
   *
   * @var string
   */
  protected $errorCode;

  /**
   * Possible error causes.
   *
   * @var array
   */
  protected $errorCauses;


  /**
   * State token.
   *
   * @var string
   */
  protected $stateToken;

  /**
   * Response constructor.
   *
   * @param object $response
   *   API response.
   */
  public function __construct($response) {
    $properties = [
      'errorSummary',
      'errorCode',
    ];
    foreach ($properties as $property) {
      $this->$property = $this->getValue($response, $property);
    }

    if (!empty($response->errorCauses)) {
      foreach ($response->errorCauses as $error) {
        $this->errorCauses[] = $error->errorSummary;
      }
    }

  }

  /**
   * Get error message.
   *
   * @return null|string
   *   Error message.
   */
  public function getError() {
    return $this->errorSummary;
  }

  /**
   * Get error code.
   *
   * @return null|string
   *   Error code.
   */
  public function getErrorCode() {
    return $this->errorCode;
  }

  /**
   * Get value from object.
   *
   * @param object $object
   *   API response object.
   * @param string $property
   *   Object property.
   * @param null|string $default_value
   *   Default value to return if there is no value or key.
   *
   * @return null|string
   *   Value.
   */
  protected function getValue($object, $property, $default_value = NULL) {
    return $object->$property ?? $default_value;
  }

  /**
   * Get error causes.
   *
   * @return null|array
   *   Error causes.
   */
  public function getErrorCauses() {
    return $this->errorCauses;
  }

}
