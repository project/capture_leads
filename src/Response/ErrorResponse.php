<?php

namespace Drupal\capture_leads\Response;

/**
 * Capture Leads Error response.
 *
 * @package Drupal\capture_leads\Response
 */
class ErrorResponse extends Response {

}
