
# Capture Leads

## Installation

To install this module, `composer require` it, or  place it in your modules
folder and enable it on the modules page.


## Configuration

All settings for this module are on the Capture Leads configuration page, under the
Configuration section. You can visit the configuration page directly at 
admin/config/capture_leads that includes two fields at this time.

Redirect Domain is the domain only that will be used to redirect visitors.

Debug Mode is a boolean that should return verbose debugging info in error messages.

 ** Enabling Debug Mode on the configuration form will cause error messages that may
include information that compromise security.  It is highly recommended that Debug Mode
be disabled in production environments.

## Testing

Capture Leads is a genericized version of a proprietary custom module created for a limited use-case.

It is a phase I project that provides an API endpoint. The ONLY thing it does at this time is to redirect 
the user who hits the endpoint to the redirect domain set in the config form with the path included in the
redirectUrl value posted to the endpoint.

Phase II will include going back to the 3rd party partner with the memberId to capture and store additional
data about the user as a marketing lead before redirecting to the product related to the button they clicked.

An outside site should point a form button at the endpoint and include the following hidden form values 
as part of a POST request.  The values, of course, can be changed.

"memberId": "1313" -> int
"cellCode": "y85lw7fue896" -> varchar(12)
"campaignCode": "5478idp9eh85" -> varchar(12)
"redirectUrl": "/lifestages" -> varchar(255) ** PATH only.

Please examine the included Postman collection as well.

## Credit

The Capture Leads module was originally developed in April 2021 by Jee Elmore of [Serendipities.net](https://serendipities.net/).
