<?php

namespace Drupal\capture_leads\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the capture_leads module.
 */
class APIControllerTest extends WebTestBase {

  /**
   * Drupal\Core\Routing\RedirectDestinationInterface definition.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "capture_leads APIController's controller functionality",
      'description' => 'Test Unit for module capture_leads and controller APIController.',
      'group' => 'Other',
    ];
  }

  /**
   * Tests capture_leads functionality.
   */
  public function testDefaultController() {
    // Check that the basic functions of module capture_leads.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
